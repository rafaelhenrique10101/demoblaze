#language:pt
#encoding: iso-8859-1

@SiteDemoblaze
Funcionalidade: Teste de aplicacoes no site de compras demoblaze.com

  @realizarContato
  Cenario: Realizar contato
    Dado que o cliente acesse o site "https://demoblaze.com/"
    Quando o cliente enviar a mensagem preenchendo os dados <e-mail>, <nome>, <mensagem>
    Entao um alerta sera exibido informando que a mensagem <mensagem_alerta> foi enviada com sucesso

    Exemplos:
      | e-mail                | nome                | mensagem                                      | mensagem_alerta				|
      | "olegario@gmail.com"  | "Olegario Silva"    | "Gostaria de saber sobre reserva de produto"  | "Thanks for the message!!"	|


  @removerProduto
  Cenario: Remover produto
    Dado que o cliente acesse o site "https://demoblaze.com/"
    Quando ele adiciona um produto ao carrinho
    #E depois remove esse produto
    #Entao o produto nao sera listado no carrinho


  @realizarPedido
  Cenario: Realizar pedido
    Dado que o cliente acesse o site "https://demoblaze.com/"
    Quando ele adiciona um produto ao carrinho
    E preenche os dados de compra <nome>, <pais>, <cidade>, <numero_cartao>, <mes>
    Entao ao realizar a compra sera exibida uma mensagem com todas as informacoes

    Exemplos:
      | nome              | pais      | cidade          | numero_cartao  			| mes   | ano 	  |
      | "Olegario Silva"  | "Brasil"  | "Passa e Fica"  | "5586 8956 9588 5898"	    | "09"  | "2028"  |