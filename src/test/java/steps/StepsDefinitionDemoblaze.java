package steps;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pages.*;
import io.cucumber.java.pt.*;
import static steps.Environment.wait;


public class StepsDefinitionDemoblaze{

	WebDriver driver = Environment.driver;
	BasePage page = new BasePage(driver, wait);
	ContactPage contactPage;
	CartPage cartPage;
	ProductPage productPage = new ProductPage(driver);


	@Dado("que o cliente acesse o site {string}")
	public void que_o_cliente_acesse_o_site(String site) {
		page.navigateUrl(site);
	}

	@Quando("ele adiciona um produto ao carrinho")
	public void ele_adiciona_um_produto_ao_carrinho() {
		HomePage homePage = new HomePage(driver);
		PageFactory.initElements(driver, HomePage.class);
		homePage.addProduct();
	}

	@Quando("o cliente enviar a mensagem preenchendo os dados {string}, {string}, {string}")
	public void o_cliente_enviar_a_mensagem_preenchendo_os_dados(String email, String name, String message) {
		contactPage = PageFactory.initElements(driver, ContactPage.class);
		contactPage.registerMessage(email, name, message);
	}

	@Entao("um alerta sera exibido informando que a mensagem {string} foi enviada com sucesso")
	public void um_alerta_sera_exibido_informando_que_a_mensagem_foi_enviada_com_sucesso(String message) {
		contactPage = PageFactory.initElements(driver, ContactPage.class);
		String msgReturnedMessage = contactPage.getAlertMessage(message);
		Assert.assertTrue(msgReturnedMessage.contains(message));
	}

	@Entao("o produto nao sera listado no carrinho")
	public void o_produto_nao_sera_listado_no_carrinho() {

	}

	@E("depois remove esse produto")
	public void depois_remove_esse_produto() {
		cartPage.cartAccess();
	}

	@E("preenche os dados de compra {string}, {string}, {string}, {string}, {string}")
	public void preenche_os_dados_de_compra(String nome, String pais, String cidade, String numeroCartao, String mes) {

	}

	@Entao("ao realizar a compra sera exibida uma mensagem com todas as informacoes")
	public void ao_realizar_a_compra_sera_exibida_uma_mensagem_com_todas_as_informacoes() {

	}
}
	    