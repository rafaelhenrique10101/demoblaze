package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import steps.Environment;

import java.util.List;


public class HomePage{

    public WebDriver driver;
    public BasePage page;
    public ProductPage productPage;

    By products = By.xpath("//div[@id='tbodyid']/div/div/div/h4");

    public HomePage(WebDriver driver) {
        this.driver = driver;
        this.page = new BasePage(this.driver, Environment.wait);
    }

    public void addProduct(){
        productPage = new ProductPage(this.driver);
        page.waitLoadElements(products);

        List<WebElement> elProducts = driver.findElements(products);

        for(int i = 0; i < elProducts.size(); i++){
            page.waitLoadElements(products);
            List<WebElement> listProducts = driver.findElements(products);
            WebElement linkProduct = listProducts.get(i).findElement(By.tagName("a"));

            page.clickElement(linkProduct);

            PageFactory.initElements(driver, ProductPage.class);
            productPage.addProductCart();
        }
    }
}
