package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BasePage{

	public WebDriver driver;
	public WebDriverWait wait;


	public BasePage(WebDriver driver, WebDriverWait wait){
		this.driver = driver;
		this.wait = wait;
	}

	public void navigateUrl(String site) {
		driver.get(site);
		wait.until(ExpectedConditions.urlContains(site));
	}

	public void waitLoadElements(By locator){
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
	}

	public void waitLoadedUrl(String url){
		wait.until(ExpectedConditions.urlContains(url));
	}

	public void sendkeys(By locator, String texto) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		WebElement element = driver.findElement(locator);
		element.sendKeys(texto);
	}

	public void click(By locator) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		WebElement element = driver.findElement(locator);
		element.click();
	}

	public void clickElement(WebElement element){
		wait.until(ExpectedConditions.visibilityOf(element));
		element.click();
	}

	public String validateAlertMessage(String message) {
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();

		String msgReturnedAlert = alert.getText();
		alert.accept();

		return msgReturnedAlert;
	}

	public void alertAccept() {
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
}
