package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import steps.Environment;


public class ProductPage {

    public WebDriver driver;
    public BasePage page;

    By btnAdicionarProdutoCarrinho = By.xpath("//a[text()='Add to cart']");
    By btnLogo = By.id("nava");

    public ProductPage(WebDriver driver) {
        this.driver = driver;
        this.page = new BasePage(this.driver, Environment.wait);
    }

    public void addProductCart(){

        page.waitLoadedUrl("https://demoblaze.com/prod.html");
        page.click(btnAdicionarProdutoCarrinho);
        page.alertAccept();
        page.click(btnLogo);
    }
}
