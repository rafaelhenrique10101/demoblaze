package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import steps.Environment;

public class ContactPage {

	public WebDriver driver;
	public BasePage page;

	By linkContact = By.xpath("//a[text()='Contact']");
	By emailUser = By.id("recipient-email");
	By nameUser = By.id("recipient-name");
	By messageUser = By.id("message-text");
	By btnSendMenssage = By.xpath("//button[text()='Send message']");

	public ContactPage(WebDriver driver) {
		this.driver = driver;
		this.page = new BasePage(this.driver, Environment.wait);
	}
	
	public void registerMessage(String email, String name, String mensagem) {

		page.click(linkContact);
		page.sendkeys(emailUser, email);
		page.sendkeys(nameUser, name);
		page.sendkeys(messageUser, mensagem);

		page.click(btnSendMenssage);

	}
	
	public String getAlertMessage(String message) {
		return page.validateAlertMessage(message);
	}
}
