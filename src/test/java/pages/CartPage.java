package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import steps.Environment;

public class CartPage {

    public WebDriver driver;
    public BasePage page;

    //@FindBy(xpath = "//*[@id=\"tbodyid\"]/tr")
    //WebElement produtoCarrinho;

    By linkCart = By.xpath("//a[text()='Cart']");

    public CartPage(WebDriver driver) {
        this.driver = driver;
        this.page = new BasePage(this.driver, Environment.wait);
    }

    public void cartAccess(){
        page.click(linkCart);
    }


}
